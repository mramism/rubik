from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'mainapp/home.html')

def contact(request):
    return render(request, 'mainapp/basic.html', {"values": ["Если у вас остались вопросы, то задавайте их мне по телефону", "8(800)555 3535", "example@mail.com"]})
